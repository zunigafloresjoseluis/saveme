var express=require('express'),
    routes=express.Router(),
    User=require('../models/Usuario')
    Puntos_Delitos=require('../models/Delitos'),
    validacion=require('../validaciones/validacion'),
    cb=new validacion(),
    formula=require('../validaciones/formula'),
    haversine=new formula(),
    sequealize=require('../postgresql_connect'),
    Tipo_delito=require('../models/Tipo_delito'),
    Usuario_delito=require('../models/Usuario_delito'),
    Datos_vivienda=require('../models/Datos_vivienda')
   


routes 
    .post('/login',(req,res,next)=>{
           if(req.body.username!=undefined && req.body.password!=undefined){
               User.findOne({
                   where:{ 
                       username:req.body.username,
                       password:req.body.password,
                   }
               }             
               )
               .then(data=>{
                    console.log(data)
                    let respuesta={};
                    if(data!=null){
                       respuesta.estado="Acceptado"
                       respuesta.err=null
                          
                    }
                    else{
                        respuesta.estado="Rechazado"
                        respuesta.err="Contraseña o Usuario Invalido"

                    }

                    switch(cb.cabeceras(req.headers.accept)){
                        case 'application/json':
                            res.setHeader('Content-Type', 'application/json');
                            res.json(200,respuesta);
                            break;
                        default:
                            res.status(406);
                            res.end();
                            break;
                    }

               })
               .catch(err=>{
                res.setHeader('Content-Type', 'application/json');
                res.json(500,{err:"error de servidor"});
               })
           }

    })
    .put('/usuario',(req,res,next)=>{
        let obj_update=req.body.updates;
        return sequealize.transaction(trans=>{
                return User.update({
                    obj_update
                },{where:{username:req.body.username}},{transaction:trans})
            })
            .then(data=>{
                res.status(201);
                res.end();
                next();
             })
             .catch(err=>{
                console.log(err.stack)
                res.status(400);
                res.end();
                next();
             })

    })
    .post('/puntos',(req,res,next)=>{
         const rango=parseInt(req.body.rango)|| 4;
         
        const inicial={
            latitude:parseInt(req.body.latitud),
            longitude:parseInt(req.body.longitud)
        }
       
        Distancias={distancias:[],punto:[]}
        Puntos_Delitos.findAll()
        .then(data=>{
         //   console.log(data[0].dataValues)
            for(let i=0;i<data.length;i++){
                let final={
                    latitude:data[i].dataValues.latitud,
                    longitude:data[i].dataValues.longitud
                }
                let distancia_original=haversine.getKilometros(inicial.latitude,inicial.longitude,final.latitude,final.longitude)
                if (distancia_original<rango){
                    Distancias.distancias.push()
              
           
                    Distancias.punto.push(final)
    
                }
               
            }
            switch(cb.cabeceras(req.headers.accept)){
                case 'application/json':
                    res.setHeader('Content-Type', 'application/json');
                    res.json(200,Distancias);
                    break;
                default:
                    res.status(406);
                    res.end();
                    break;
            }

        })
        .catch(err=>{
            res.setHeader('Content-Type', 'application/json');
            res.json(500,{err:"error de servidor"});
            }
        )

    })
    .post('/agregar',(req,res,next)=>{
                let datos={
                    usuario:req.body.username,
                    password:req.body.password,
                    nombre_tipo_delito:req.body.tipo_delito,
                    descripcion_delito:req.body.descripcion_delito,
                    nombre_delito:req.body.nombre_delito,
                    descripcion_delito:req.body.descripcion_delito,
                    fecha:req.body.fecha, 
                    hora:req.body.hora,riesgo:req.body.riesgo,
                    latitud:req.body.latitud,
                    longitud:req.body.longitud
                }
            User.findOne({
                where:{ 
                    username:req.body.username,
                    password:req.body.password,
                }
            })
            .then(
                Tipo_delito.findOne({
                    where:{
                        nombre:datos.nombre_delito, 
                        descripcion:datos.descripcion_delito
                    }
                })
                .then(data=>{
                    if (data!=null){
                    return sequealize.transaction(trans=>{ 
                        return Puntos_Delitos.create({
                            id_tipo:data.id_tipo,
                            nombre:datos.nombre_delito,
                            descripcion:datos.descripcion_delito,
                            fecha:new Date(),
                            hora:datos.hora,
                            latitud:datos.latitud,
                            longitud:datos.longitud
                        },{transaction:trans})
                        .then(
                            Puntos_Delitos.findOne({
                                where:{           
                                    id_tipo:data.id_tipo,
                                    nombre:datos.nombre_delito,
                                    descripcion:datos.descripcion_delito,
                                    fecha:new Date(),
                                    hora:datos.hora,
                                    latitud:datos.latitud,
                                    longitud:datos.longitud
                                }
                                })
                                .then(data=>{
                                    return sequealize.transaction(trans=>{
                                        return Usuario_delito.create({
                                            username:datos.usuario,
                                            id_localizacion:data.id_localizacion
                                        },{transaction:trans})
                                        .then(data=>{
                                            res.status(201);
                                            res.end();
                                            next();
                                        } )
                                    })
                                }
                                
                                )
                        )  
                        })
                    }
                    else{
                        res.status(404);
                        res.end();
                        next();

                    }
                }

                )
                .catch(err=>{
                    res.status(404);
                    res.end();
                    next();
                        console.log(err.stack)
                        
                }

                )

        )
        .catch(err=>{
            res.status(404);
                    res.end();
                    next();
        }

        )

    })
    .post('/register',(req,res,next)=>{
            return sequealize.transaction(trans=>{
                 return User.create({
                     username:req.body.username,
                     password:req.body.password,
                     nombre:req.body.nombre,
                     app_p:req.body.apellido_paterno,
                     app_m:req.body.apellido_materno,
                     numero:req.body.numero_telefonico,
                     correo:req.body.correo
                 },{transaction:trans})
                 .then(data=>{
                    res.status(201);
                    res.end();
                    next();
                 })
                 .catch(err=>{
                    console.log(err.stack)
                    res.status(400);
                    res.end();
                    next();
                 })
            })
    })
    .post('/get-datos-vivienda',(req,res,next)=>{
        
        Datos_vivienda.findOne({
            where:{
                username:req.body.username
            }
        })
        .then(data=>{
        let  respuesta={
            username:data.username,
            colonia:data.colonia,
            numero_exterior:data.numero_exterior,
            calle:data.calle,
            cp:data.cp
            }
            switch(cb.cabeceras(req.headers.accept)){
                case 'application/json':
                    res.setHeader('Content-Type', 'application/json');
                    res.json(200,respuesta);
                    break;
                default:
                    res.status(406);
                    res.end();
                    break;
            }
        
        })
        .catch(err=>{
            console.log(err.stack)
            res.status(404);
            res.end();
            next();
        })
    })
    .post('/datos-vivienda',(req,res,next)=>{
        return sequealize.transaction(trans=>{
          
            return Datos_vivienda.create({
              username:req.body.username,
              colonia:req.body.colonia || ' ',
              numero_exterior:parseInt(req.body.numero_exterior)|| -1,
              calle:req.body.calle||' ',
              cp:parseInt(req.body.cp) || 0
            },{transaction:trans})
            .then(data=>{
                res.status(201);
                res.end();
                next();
             })
             .catch(err=>{
                console.log(err.stack)
                res.status(400);
                res.end();
                next();
             })
        })
    })
    .put('/datos-vivienda',(req,res,next)=>{
            let obj_update=req.body.updates;
            return sequealize.transaction(trans=>{
                    return Datos_vivienda.update({
                        obj_update
                    },{where:{username:req.body.username}},{transaction:trans})
                })
                .then(data=>{
                    res.status(201);
                    res.end();
                    next();
                 })
                 .catch(err=>{
                    console.log(err.stack)
                    res.status(400);
                    res.end();
                    next();
                 })
    })
    .delete('/user',(req,res,next)=>{
        let cuenta=req.body.username
        return sequealize.transaction(trans=>{
            return Usuario_delito.destroy({
                where:{
                    username:cuenta
                }
            },{transaction:trans})
            .then(data=>{
                return sequealize.transaction(trans=>{
                    return Datos_vivienda.destroy({
                        where:{
                            username:cuenta
                        }
                    },{transaction:trans})
                    .then(data=>{
                        return sequealize.transaction(trans=>{
                            return User.destroy({
                                where:{
                                    username:cuenta
                                }
                            },{transaction:trans})
                        })
                        .then(data=>{
                        res.status(204);
                        res.end();
                        next()
                        })
                    })
                })
            })
            .catch(err=>{
                console.log(err.stack)
               
                res.status(400);
                res.end();
                next();
            })
        })
    })

    .get('/',(req,res,next)=>{
        res.send('adasd')

    })

module.exports=routes