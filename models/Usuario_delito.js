
var Sequelize=require('sequelize')
    connection=require('../postgresql_connect')

const Usuario_delito=connection.define('usuario_delito',{
    username:{
        type:Sequelize.STRING,
        field:"username",
        allowNull:false,
        primaryKey:true
    },
    id_localizacion:{
       type:Sequelize.INTEGER,
       field:"id_localizacion"
    }
    },{
    timestamps:false,
    freezeTableName:false,
    tableName:"usuario_delitos"
    })

module.exports=Usuario_delito