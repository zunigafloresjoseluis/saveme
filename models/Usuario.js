var Sequelize=require('sequelize'),
    connection=require('../postgresql_connect')
const Usuario=connection.define('usuario',{
       
        username:{
            type:Sequelize.STRING,
            field:"username",
            allowNull:false,
            primaryKey:true
        },
        password:{
                type:Sequelize.STRING,
                field:"password"
        },
        nombre:{
            type:Sequelize.STRING,
            field:"nombre"
        },
        app_p:{
            type:Sequelize.STRING,
            field:"app_p"
        },
        app_m:{
            type:Sequelize.STRING,
            field:"app_m"
        },
        numero_telefonico:{
            type:Sequelize.STRING,
            field:"numero_telefonico"
        },
        correo:{
            type:Sequelize.STRING,
            field:"correo"
        }
    
        },{
            timestamps: false,
          freezeTableName: false,
          tableName: 'usuario'
      }
    )

module.exports=Usuario