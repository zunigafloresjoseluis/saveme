var Sequelize=require('sequelize'),
    connection=require('../postgresql_connect')
const Delitos=connection.define('delitos',{
    id_localizacion:{
        type:Sequelize.INTEGER,
        field:"id_localizacion",
        primaryKey:true,
        allowNull:false,
        autoIncrement:true
       

    },
    id_tipo:{
        type:Sequelize.INTEGER,
        field:"id_tipo"
    },
    nombre:{
        type:Sequelize.STRING,
        field:"nombre"
     },
    descripcion:{
        type:Sequelize.STRING,
        field:"descripcion"
    },
    fecha:{
        type:Sequelize.DATE,
        field:"fecha"
    },
    hora:{
        type:Sequelize.STRING,
        field:"hora"
    },
    riesgo:{
        type:Sequelize.STRING,
        field:"riesgo"
    },
    latitud:{
        type:Sequelize.NUMERIC,
        field:"latitud"
    },
    longitud:{
        type:Sequelize.NUMERIC,
        field:"longitud"

    }



    },{
        timestamps: false,
      freezeTableName: false,
      tableName: 'delitos'
  }
)
module.exports=Delitos