
var Sequelize=require('sequelize'),
    connection=require('../postgresql_connect')

const Datos_vivienda=connection.define('datos_vivienda',{
    username:{
        type:Sequelize.STRING,
        field:"username",
        allowNull:false,
        primaryKey:true
    },
    colonia:{
        type:Sequelize.STRING,
        field:"colonia"
    },
    numero_exterior:{
        type:Sequelize.NUMERIC,
        field:"numero_exterior"
    },
    calle:{
        type:Sequelize.STRING,
        field:"calle"
    },
    cp:{
        type:Sequelize.NUMERIC,
        field:"cp"
    }

    } ,
    {
    timestamps:false,
    freezeTableName:false,
    tableName:"datos_vivienda"
    })


module.exports=Datos_vivienda