var Sequelize=require('sequelize'),
    connection=require('../postgresql_connect')
const Tipo_delito=connection.define('tipos_delito',{
    id_tipo:{
        type:Sequelize.INTEGER,
         
            field:"id_tipo",
           primaryKey:true,
            allowNull:false,
          
    },
    nombre:{
        type:Sequelize.STRING,
        field:"nombre"
    },
    descripcion:{
        type:Sequelize.STRING,
        field:"descripcion"
    }


},{
    timestamps: false,
  freezeTableName: false,
  tableName: 'tipo_delito'
})

module.exports=Tipo_delito