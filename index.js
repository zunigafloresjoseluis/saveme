var express=require('express'),
	favicon=require('serve-favicon'),
	bodyParser=require('body-parser'),
	morgan=require('morgan'),
	restful=require('express-method-override')('_method'),
	icon_favicon=(`${__dirname}/Alpify.png`),
	port=(process.port)||3000,
	routes=require('./routes/routes'),
	app=express(),
	foraneas = require('./foraneas'),
	cookieParser = require('cookie-parser'),
	session = require('express-session')


app
	.set('port',port)
	.use(bodyParser.urlencoded({ extended: false }))
     .use(bodyParser.json())
	.use(restful)
	.use(favicon(icon_favicon))
	.use(morgan('dev'))
	.use(
		function crossOrigin(req,res,next){
			res.header("Access-Control-Allow-Origin","*");
			res.header("Access-Control-Allow-Headers","X-Requested-With");
			return next();
		}
	)
	.use(routes)
	

	app.use(session({
		secret: '2C44-4D44-WppQ38S',
		resave: true,
		saveUninitialized: true
	}));
module.exports=app;
