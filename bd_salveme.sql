﻿
-- #Datos_delictivos;
CREATE TABLE delitos(id_localizacion  SERIAL  primary key,id_tipo int , nombre varchar,descripcion varchar,fecha date, hora varchar,riesgo varchar,latitud decimal,longitud decimal);

CREATE TABLE tipo_delito(id_tipo SERIAL primary key,nombre varchar, descripcion varchar);

CREATE TABLE usuario(username varchar primary key,password varchar,nombre varchar,app_p varchar,app_m varchar, numero_telefonico varchar,correo varchar );

CREATE TABLE usuario_delitos(username varchar, id_localizacion int);

CREATE TABLE datos_vivienda(username varchar, colonia varchar,numero_exterior numeric,calle varchar,cp numeric);

