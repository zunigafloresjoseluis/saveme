
alter table delitos
 add constraint tipo_delito
 foreign key (id_tipo)
 references tipo_delito(id_tipo);


alter table usuario_delitos
add constraint usuario_delito
foreign key (username)
references usuario(username);

alter table datos_vivienda
add constraint vivienda
foreign key (username)
references usuario(username);
